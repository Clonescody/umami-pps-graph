# cmUMAMI ratio tracker

Simple serverless SPA to display the historical cmUMAMI:UMAMI ratio. 

Uses UMAMI official [hosted subgraph](https://github.com/UmamiDAO/subgraph) to track the ratio after each `reinvest` event.

DM me on twitter [Clonescody](https://twitter.com/Clonescody) or on Discord Clonescody#1164 for any issue/question.

Run locally : `npm install && npm run start`
