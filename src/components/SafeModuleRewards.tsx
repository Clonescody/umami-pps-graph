import React, { useEffect, useState } from "react";
import moment from "moment";
import { Flex, Spinner, Text } from "@chakra-ui/react";
import {
  CartesianGrid,
  ComposedChart,
  Legend,
  Line,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

import { apolloClient } from "../App";
import { GraphGlpWethRewardSafeModule } from "../types/glpWethRewardSafeModule";
import { glpWethRewardsDistributedsQuery } from "../queries/glpWethRewardsDistributed";
import { fillInMissingGlpSafeRewardsGraphObjects } from "../utils/helpers";

const SafeModuleRewardsGraph = () => {
  const [safeModuleRewards, setSafeModuleRewards] = useState<
    Array<GraphGlpWethRewardSafeModule>
  >([]);

  const fetchSafeModuleRewards = async (): Promise<void> => {
    apolloClient
      .query({
        query: glpWethRewardsDistributedsQuery,
      })
      .then((data) => {
        const distributionsResponse: Array<GraphGlpWethRewardSafeModule> =
          data.data.glpWethRewardsDistributeds;
        const sortedDistributions = distributionsResponse.map((entity) => ({
          ...entity,
          totalClaimed: (Number(entity.totalClaimed) / 10 ** 18).toFixed(6),
          amountDistributed: (
            Number(entity.amountDistributed) /
            10 ** 18
          ).toFixed(6),
          timestamp: `${parseInt(entity.timestamp) * 1000}`,
        }));
        setSafeModuleRewards(
          fillInMissingGlpSafeRewardsGraphObjects(sortedDistributions)
        );
      })
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    if (safeModuleRewards.length === 0) {
      fetchSafeModuleRewards();
    }
  });

  if (safeModuleRewards.length === 0) {
    return (
      <Flex
        flexDirection={"column"}
        justifyContent={"center"}
        alignItems={"center"}
        width={"100%"}
      >
        <Spinner size={"lg"} color="blue.600" />
      </Flex>
    );
  }

  return (
    <Flex flexDirection={"column"}>
      <Text>
        {`${safeModuleRewards.reduce(
          (acc, { totalClaimed }) => acc + parseFloat(totalClaimed),
          0
        )} ETH claimed`}
      </Text>
      <Text>
        {`${safeModuleRewards.reduce(
          (acc, { amountDistributed }) => acc + parseFloat(amountDistributed),
          0
        )} ETH distributed`}
      </Text>
      <ComposedChart height={400} width={800} data={safeModuleRewards}>
        <Line
          type="monotone"
          dataKey="totalClaimed"
          stroke="#6736b5"
          yAxisId={0}
          dot={false}
        />
        <Line
          type="monotone"
          dataKey="amountDistributed"
          stroke="#8884d8"
          dot={false}
        />

        <CartesianGrid strokeDasharray="5 5" />
        <XAxis
          dataKey="timestamp"
          tickFormatter={(timestamp) =>
            moment(parseInt(timestamp)).format("YYYY-MM-DD")
          }
        />
        <YAxis
          yAxisId={0}
          dataKey="totalClaimed"
          domain={[
            0,
            parseInt(
              `${
                Math.max(
                  ...safeModuleRewards.map(({ totalClaimed }) =>
                    parseFloat(totalClaimed)
                  )
                ) + 1
              }`,
              10
            ),
          ]}
        />
        <Legend />
        <Tooltip
          labelFormatter={(label) =>
            moment(parseInt(label)).format("YYYY-MM-DD")
          }
        />
      </ComposedChart>
    </Flex>
  );
};

export default SafeModuleRewardsGraph;
