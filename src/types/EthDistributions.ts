export type GraphQueryEthDistribution = {
  id: string;
  block: string;
  timestamp: string;
  txHash: string;
  ethDistributed: string;
};

export type EthDistribution = GraphQueryEthDistribution & {};

export type EthDistributionGraphObject = GraphQueryEthDistribution & {
  ethCumulative: string;
  movingAverage: string | null;
};
