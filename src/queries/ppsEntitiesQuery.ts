import { gql } from "@apollo/client";

export const ppsEntitiesQuery = gql`
  query {
    ppsEntities(first: 1000, orderBy: timestamp) {
      id
      block
      timestamp
      txHash
      pricePerShare
    }
  }
`;
