import "./App.css";
import React from "react";
import { ChakraProvider, Flex, Heading } from "@chakra-ui/react";
import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";

import PpsGraph from "./components/PpsGraph";
import { CM_UMAMI_GRAPH_URL } from "./utils/constants";
import EthDistributionsGraph from "./components/EthDistributionsGraph";
import EthDistributionHistory from "./components/EthDistributionHistory";
import Informations from "./components/Informations";
import SafeModuleRewardsGraph from "./components/SafeModuleRewards";
import SafeModuleRewardsHistory from "./components/SafeModuleRewardsHistory";

export const apolloClient = new ApolloClient({
  link: new HttpLink({
    uri: CM_UMAMI_GRAPH_URL,
  }),
  cache: new InMemoryCache(),
});

const App = (): JSX.Element => {
  return (
    <ChakraProvider>
      <Flex minHeight={"100vh"} width={"100%"} p={"1rem"}>
        <Flex
          flexDirection={"column"}
          justifyContent={"flex-start"}
          alignItems={"flex-start"}
          width={"100%"}
        >
          <Heading>Informations</Heading>
          <Informations />
          <Heading>cmUMAMI Price per share</Heading>
          <PpsGraph />
          <Heading>ETH distributions</Heading>
          <EthDistributionsGraph />
          <Heading>Distribution history</Heading>
          <EthDistributionHistory />
          <Heading>Safe Module GLP rewards claims</Heading>
          <SafeModuleRewardsGraph />
          <Heading>Safe Module claims history</Heading>
          <SafeModuleRewardsHistory />
        </Flex>
      </Flex>
    </ChakraProvider>
  );
};

export default App;
