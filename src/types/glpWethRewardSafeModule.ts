export type GraphGlpWethRewardSafeModule = {
  id: string;
  block: string;
  timestamp: string;
  txHash: string;
  totalClaimed: string;
  amountDistributed: string;
};
