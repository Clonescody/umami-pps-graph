import moment from "moment";
import {
  EthDistributionGraphObject,
  GraphQueryEthDistribution,
} from "../types/EthDistributions";
import { GraphGlpWethRewardSafeModule } from "../types/glpWethRewardSafeModule";
import { GraphQueryPpsEntity, PpsEntity } from "../types/PpsEntity";
import { CM_UMAMI_STARTING_TIMESTAMP } from "./constants";

export const fillInMissingPps = (
  entitiesArray: Array<GraphQueryPpsEntity>
): Array<PpsEntity> => {
  let finalArray: Array<PpsEntity> = [];
  const startDate = moment(CM_UMAMI_STARTING_TIMESTAMP);
  const daysUntilToday = moment().diff(startDate, "day");

  for (let i = 0; i <= daysUntilToday; i += 1) {
    const loopDate = moment(startDate).add(i, "day");

    if (loopDate.isSameOrBefore(startDate)) {
      finalArray.push({
        id: `${loopDate.unix() * 1000}`,
        timestamp: `${loopDate.unix() * 1000}`,
        pricePerShare: "1.0",
        block: "",
        txHash: "",
      });
    } else {
      const existingEntity = entitiesArray.find((entity) =>
        moment(parseInt(entity.timestamp)).isSame(loopDate, "day")
      );
      if (existingEntity) {
        finalArray.push(existingEntity);
      } else {
        finalArray.push({
          ...finalArray[finalArray.length - 1],
          id: `${loopDate.unix() * 1000}`,
          timestamp: `${loopDate.unix() * 1000}`,
        });
      }
    }
  }

  return finalArray;
};

export const fillInMissingGlpSafeRewardsGraphObjects = (
  distributionsArray: Array<GraphGlpWethRewardSafeModule>
): Array<GraphGlpWethRewardSafeModule> => {
  const startDate = moment(1677164893000);
  const daysUntilToday = moment().diff(startDate, "day");
  let finalArray: Array<GraphGlpWethRewardSafeModule> = [];

  for (let i = 0; i <= daysUntilToday; i += 1) {
    const loopDate = moment(startDate).add(i, "day");
    let finalLoopObject: GraphGlpWethRewardSafeModule = {
      id: `${loopDate.unix() * 1000}`,
      timestamp: `${loopDate.unix() * 1000}`,
      block: "",
      txHash: "",
      totalClaimed: "0.0",
      amountDistributed: "0.0",
    };
    if (!loopDate.isSameOrBefore(startDate)) {
      const dailyDistributions = distributionsArray.filter((entity) =>
        moment(parseInt(entity.timestamp)).isSame(loopDate, "day")
      );
      if (dailyDistributions.length > 0) {
        const totalClaimed = `${dailyDistributions.reduce(
          (a, { totalClaimed }) => a + parseFloat(totalClaimed),
          0
        )}`;
        const totalAmountDistributed = `${dailyDistributions.reduce(
          (a, { amountDistributed }) => a + parseFloat(amountDistributed),
          0
        )}`;
        finalLoopObject = {
          ...finalLoopObject,
          totalClaimed,
          amountDistributed: totalAmountDistributed,
        };
      } else {
        finalLoopObject = {
          ...finalArray[finalArray.length - 1],
          id: `${loopDate.unix() * 1000}`,
          timestamp: `${loopDate.unix() * 1000}`,
          totalClaimed: "0.0",
          amountDistributed: "0.0",
        };
      }
    }
    finalArray.push(finalLoopObject);
  }

  return finalArray;
};

export const fillInMissingDistributionsGraphObjects = (
  distributionsArray: Array<GraphQueryEthDistribution>
): Array<EthDistributionGraphObject> => {
  const startDate = moment(1647375646000);
  const daysUntilToday = moment().diff(startDate, "day");
  const SMA = 14;
  let finalArray: Array<EthDistributionGraphObject> = [];

  for (let i = 0; i <= daysUntilToday; i += 1) {
    const loopDate = moment(startDate).add(i, "day");
    let finalLoopObject: EthDistributionGraphObject = {
      id: `${loopDate.unix() * 1000}`,
      timestamp: `${loopDate.unix() * 1000}`,
      block: "",
      txHash: "",
      ethDistributed: "0.0",
      ethCumulative: "0.0",
      movingAverage: null,
    };
    if (!loopDate.isSameOrBefore(startDate)) {
      const dailyDistributions = distributionsArray.filter((entity) =>
        moment(parseInt(entity.timestamp)).isSame(loopDate, "day")
      );
      if (dailyDistributions.length > 0) {
        const ethDistributed = `${dailyDistributions.reduce(
          (a, { ethDistributed }) => a + parseFloat(ethDistributed),
          0
        )}`;
        finalLoopObject = {
          ...finalLoopObject,
          ethDistributed,
          ethCumulative: `${
            parseFloat(ethDistributed) +
            parseFloat(finalArray[finalArray.length - 1].ethCumulative)
          }`,
        };
      } else {
        finalLoopObject = {
          ...finalArray[finalArray.length - 1],
          id: `${loopDate.unix() * 1000}`,
          timestamp: `${loopDate.unix() * 1000}`,
          ethDistributed: "0.0",
        };
      }
    }
    finalArray.push(finalLoopObject);
    if (i > SMA) {
      finalArray[
        finalArray.length - 1
      ].movingAverage = `${getDailyMovingAverage(finalArray, SMA)}`;
    }
  }

  return finalArray;
};

const getDailyMovingAverage = (
  previousDistributions: Array<EthDistributionGraphObject>,
  movingAverage: number
): number => {
  return (
    previousDistributions
      .slice(
        previousDistributions.length - movingAverage,
        previousDistributions.length
      )
      .reduce((a, { ethDistributed }) => a + parseFloat(ethDistributed), 0) /
    movingAverage
  );
};
