import React from "react";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Flex, Link, Text } from "@chakra-ui/react";

const Informations = (): JSX.Element => (
  <>
    <Text>
      This website is community made and not officially maintained by the UMAMI
      Finance team.
    </Text>
    <Link href="https://umami.finance/" target={"_blank"}>
      <Flex
        flexDirection={"row"}
        justifyContent="flex-start"
        alignItems={"center"}
      >
        <Text>UMAMI Finance</Text>
        <ExternalLinkIcon ml="0.2rem" />
      </Flex>
    </Link>
    <Link
      href="https://gitlab.com/Clonescody/umami-pps-graph"
      target={"_blank"}
    >
      <Flex
        flexDirection={"row"}
        justifyContent="flex-start"
        alignItems={"center"}
      >
        <Text>Visit the Gitlab repository for more details</Text>
        <ExternalLinkIcon ml="0.2rem" />
      </Flex>
    </Link>
    <Link href="https://dune.com/dbustos20/umami-finance" target={"_blank"}>
      <Flex
        flexDirection={"row"}
        justifyContent="flex-start"
        alignItems={"center"}
      >
        <Text>Visit the Dune dashboard for more statistics on UMAMI</Text>
        <ExternalLinkIcon ml="0.2rem" />
      </Flex>
    </Link>
  </>
);

export default Informations;
