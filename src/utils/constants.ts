export const CM_UMAMI_STARTING_BLOCK = 8637816;
export const CM_UMAMI_STARTING_TIMESTAMP = 1648295962 * 1000;

export const CM_UMAMI_GRAPH_URL =
  "https://api.thegraph.com/subgraphs/name/umamidao/protocol-metrics";
