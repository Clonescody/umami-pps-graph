import React, { useEffect, useState } from "react";
import {
  Button,
  Flex,
  Link,
  Spinner,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Th,
  Thead,
  Tr,
} from "@chakra-ui/react";
import {
  ChevronLeftIcon,
  ChevronRightIcon,
  ExternalLinkIcon,
} from "@chakra-ui/icons";
import moment from "moment";
import {
  EthDistribution,
  GraphQueryEthDistribution,
} from "../types/EthDistributions";
import { apolloClient } from "../App";
import { ethDistributionsQuery } from "../queries/ethDistributionsQuery";

const PAGE_OFFSET = 5;

const EthDistributionHistory = (): JSX.Element => {
  const [page, setPage] = useState(1);
  const [ethDistributions, setEthDistributions] = useState<
    Array<EthDistribution>
  >([]);

  const fetchDistributions = async (): Promise<void> => {
    apolloClient
      .query({
        query: ethDistributionsQuery,
      })
      .then((data) => {
        const distributionsResponse: Array<GraphQueryEthDistribution> =
          data.data.ethDistributions;
        const sortedDistributions = distributionsResponse.map((entity) => ({
          ...entity,
          timestamp: `${parseInt(entity.timestamp) * 1000}`,
        }));
        setEthDistributions(sortedDistributions.reverse());
      })
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    if (ethDistributions.length === 0) {
      fetchDistributions();
    }
  });

  const changePage = (newPage: number): void => {
    setPage(newPage);
  };

  if (ethDistributions.length === 0) {
    return (
      <Flex
        flexDirection={"column"}
        justifyContent={"center"}
        alignItems={"center"}
        width={"100%"}
      >
        <Spinner size={"lg"} color="blue.600" />
      </Flex>
    );
  }

  return (
    <TableContainer>
      <Table width={800} variant="simple" size={"lg"}>
        <Thead>
          <Tr>
            <Th padding={"1rem"}>Date</Th>
            <Th padding={"1rem"}>Block</Th>
            <Th padding={"1rem"}>Tx hash</Th>
            <Th padding={"1rem"} isNumeric>
              Amount
            </Th>
          </Tr>
        </Thead>
        <Tbody>
          {ethDistributions
            .slice(
              page === 1 ? 0 : (page - 1) * PAGE_OFFSET,
              page === 1 ? PAGE_OFFSET : page * PAGE_OFFSET
            )
            .map(({ timestamp, ethDistributed, txHash, block }) => (
              <Tr key={txHash}>
                <Td padding={"1rem"}>
                  {moment(parseInt(timestamp)).format("YYYY-MM-DD HH:mm:ss")}
                </Td>
                <Td padding={"1rem"}>{block}</Td>
                <Td padding={"1rem"}>
                  <Flex ml="0.5rem" flexDirection={"row"} alignItems={"center"}>
                    <Link
                      href={`https://arbiscan.io/tx/${txHash}`}
                      target="_blank"
                    >
                      <Text color="blue.400" mr="0.5rem">
                        Arbiscan
                      </Text>
                    </Link>
                    <ExternalLinkIcon />
                  </Flex>
                </Td>
                <Td padding={"1rem"} isNumeric>
                  {parseFloat(ethDistributed)} ETH
                </Td>
              </Tr>
            ))}
        </Tbody>
      </Table>
      <Flex
        flexDirection={"row"}
        justifyContent={"center"}
        alignItems={"center"}
        width={"100%"}
        mt="1rem"
      >
        <Button disabled={page === 1} onClick={() => changePage(1)}>
          <Text>First</Text>
        </Button>
        <Button
          ml="1rem"
          disabled={page === 1}
          onClick={() => changePage(page - 1)}
        >
          <ChevronLeftIcon />
        </Button>
        <Flex
          flexDirection={"row"}
          justifyContent={"space-evenly"}
          alignItems={"center"}
          width="20%"
        >
          <Text>{page === 1 ? "" : page - 1}</Text>
          <Text textDecoration={"underline"} textUnderlineOffset={5}>
            {page}
          </Text>
          <Text>
            {page * PAGE_OFFSET > ethDistributions.length ? "" : page + 1}
          </Text>
        </Flex>
        <Button
          mr="1rem"
          disabled={page * PAGE_OFFSET > ethDistributions.length}
          onClick={() => changePage(page + 1)}
        >
          <ChevronRightIcon />
        </Button>
        <Button
          disabled={
            page ===
            parseInt((ethDistributions.length / PAGE_OFFSET + 1).toString())
          }
          onClick={() =>
            changePage(
              parseInt((ethDistributions.length / PAGE_OFFSET + 1).toString())
            )
          }
        >
          <Text>Last</Text>
        </Button>
      </Flex>
    </TableContainer>
  );
};

export default EthDistributionHistory;
