import { gql } from "@apollo/client";

export const ethDistributionsQuery = gql`
  query {
    ethDistributions(first: 1000, orderBy: timestamp) {
      id
      block
      timestamp
      txHash
      ethDistributed
    }
  }
`;
