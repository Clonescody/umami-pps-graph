export type GraphQueryPpsEntity = {
  id: string;
  block: string;
  timestamp: string;
  txHash: string;
  pricePerShare: string;
};

export type PpsEntity = GraphQueryPpsEntity & {};
