import { gql } from "@apollo/client";

export const glpWethRewardsDistributedsQuery = gql`
  query {
    glpWethRewardsDistributeds(
      first: 1000
      orderBy: block
      orderDirection: desc
    ) {
      id
      block
      timestamp
      txHash
      totalClaimed
      amountDistributed
    }
  }
`;
