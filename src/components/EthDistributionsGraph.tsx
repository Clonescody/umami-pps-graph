import React, { useEffect, useState } from "react";
import moment from "moment";
import { Flex, Link, Spinner, Text } from "@chakra-ui/react";
import {
  Bar,
  CartesianGrid,
  Cell,
  ComposedChart,
  Legend,
  Line,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import {
  EthDistributionGraphObject,
  GraphQueryEthDistribution,
} from "../types/EthDistributions";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { apolloClient } from "../App";
import { ethDistributionsQuery } from "../queries/ethDistributionsQuery";
import { fillInMissingDistributionsGraphObjects } from "../utils/helpers";

const EthDistributionsGraph = () => {
  const [ethDistributionsGraphDatas, setEthDistributionsGraphDatas] = useState<
    Array<EthDistributionGraphObject>
  >([]);

  const [focusedIndex, setFocusedIndex] = useState<number>(-1);

  const fetchDistributions = async (): Promise<void> => {
    apolloClient
      .query({
        query: ethDistributionsQuery,
      })
      .then((data) => {
        const distributionsResponse: Array<GraphQueryEthDistribution> =
          data.data.ethDistributions;
        const sortedDistributions = distributionsResponse.map((entity) => ({
          ...entity,
          timestamp: `${parseInt(entity.timestamp) * 1000}`,
        }));
        setEthDistributionsGraphDatas(
          fillInMissingDistributionsGraphObjects(sortedDistributions)
        );
      })
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    if (ethDistributionsGraphDatas.length === 0) {
      fetchDistributions();
    }
  });

  const handleClick = (data: any, index: number): void => {
    setFocusedIndex(index);
  };

  if (ethDistributionsGraphDatas.length === 0) {
    return (
      <Flex
        flexDirection={"column"}
        justifyContent={"center"}
        alignItems={"center"}
        width={"100%"}
      >
        <Spinner size={"lg"} color="blue.600" />
      </Flex>
    );
  }

  return (
    <Flex flexDirection={"column"}>
      <Text>
        {`Total ETH distributed ${
          ethDistributionsGraphDatas[ethDistributionsGraphDatas.length - 1]
            .ethCumulative
        }`}
      </Text>
      <Text mb="1rem">14 days moving average</Text>
      <ComposedChart height={400} width={800} data={ethDistributionsGraphDatas}>
        <Bar
          fill={"#4299e1"}
          dataKey="ethDistributed"
          onClick={handleClick}
          yAxisId={0}
        >
          {ethDistributionsGraphDatas.map((entry, index) => (
            <Cell cursor="pointer" key={`cell-${index}`} />
          ))}
        </Bar>
        <Line
          type="monotone"
          dataKey="movingAverage"
          stroke="#6736b5"
          yAxisId={0}
          dot={false}
        />
        <Line
          type="monotone"
          dataKey="ethCumulative"
          stroke="#8884d8"
          yAxisId={1}
          dot={false}
        />

        <CartesianGrid strokeDasharray="5 5" />
        <XAxis
          dataKey="timestamp"
          tickFormatter={(timestamp) =>
            moment(parseInt(timestamp)).format("YYYY-MM-DD")
          }
        />
        <YAxis
          yAxisId={0}
          dataKey="ethDistributed"
          domain={[
            0,
            parseInt(
              `${
                Math.max(
                  ...ethDistributionsGraphDatas.map(({ ethDistributed }) =>
                    parseFloat(ethDistributed)
                  )
                ) + 1
              }`,
              10
            ),
          ]}
        />
        <YAxis
          yAxisId={1}
          dataKey="ethCumulative"
          orientation="right"
          domain={[
            0,
            parseInt(
              ethDistributionsGraphDatas[ethDistributionsGraphDatas.length - 1]
                .ethCumulative,
              10
            ) + 10,
          ]}
        />
        <Legend />
        <Tooltip
          labelFormatter={(label) =>
            moment(parseInt(label)).format("YYYY-MM-DD")
          }
        />
      </ComposedChart>
      {focusedIndex > -1 && (
        <Flex
          flexDirection={"row"}
          justifyContent={"flex-start"}
          alignItems={"center"}
        >
          <Text>
            {`${parseFloat(
              ethDistributionsGraphDatas[focusedIndex].ethDistributed
            ).toFixed(4)} ETH distributed the ${moment(
              parseInt(ethDistributionsGraphDatas[focusedIndex].timestamp)
            ).format("YYYY-MM-DD")}`}
          </Text>
          <Flex ml="0.5rem" flexDirection={"row"} alignItems={"center"}>
            <Link
              href={`https://arbiscan.io/tx/${ethDistributionsGraphDatas[focusedIndex].txHash}`}
              target="_blank"
            >
              <Text color="blue.400" mr="0.5rem">
                View on Arbiscan
              </Text>
            </Link>
            <ExternalLinkIcon />
          </Flex>
        </Flex>
      )}
    </Flex>
  );
};

export default EthDistributionsGraph;
