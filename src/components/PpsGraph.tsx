import React, { useEffect, useState } from "react";
import moment from "moment";
import { GraphQueryPpsEntity, PpsEntity } from "../types/PpsEntity";
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { Flex, Spinner, Text } from "@chakra-ui/react";
import { apolloClient } from "../App";
import { ppsEntitiesQuery } from "../queries/ppsEntitiesQuery";
import { fillInMissingPps } from "../utils/helpers";

const PpsGraph = (): JSX.Element => {
  const [ppsEntities, setPpsEntities] = useState<Array<PpsEntity>>([]);

  const fetchPps = async (): Promise<void> => {
    apolloClient
      .query({
        query: ppsEntitiesQuery,
      })
      .then((data) => {
        const entitiesResponse: Array<GraphQueryPpsEntity> =
          data.data.ppsEntities;
        const sortedEntities = entitiesResponse.map((entity) => ({
          ...entity,
          timestamp: `${parseInt(entity.timestamp) * 1000}`,
        }));
        setPpsEntities(fillInMissingPps(sortedEntities));
      })
      .catch((error) => console.error(error));
  };

  useEffect(() => {
    if (ppsEntities.length === 0) {
      fetchPps();
    }
  });

  if (ppsEntities.length === 0) {
    return (
      <Flex
        flexDirection={"column"}
        justifyContent={"center"}
        alignItems={"center"}
        width={"100%"}
      >
        <Spinner size={"lg"} color="blue.600" />
      </Flex>
    );
  }

  return (
    <Flex flexDirection={"column"}>
      <Text mb="1rem">
        Current price per share :{" "}
        {ppsEntities[ppsEntities.length - 1].pricePerShare}
      </Text>
      <LineChart height={400} width={800} data={ppsEntities}>
        <Line dataKey="pricePerShare" dot={false} />

        <CartesianGrid strokeDasharray="3 3" />
        <XAxis
          dataKey="timestamp"
          tickFormatter={(timestamp) =>
            moment(parseInt(timestamp)).format("YYYY-MM-DD")
          }
        />
        <YAxis
          domain={[
            1,
            parseFloat(
              (
                parseFloat(ppsEntities[ppsEntities.length - 1].pricePerShare) +
                0.01
              ).toFixed(2)
            ),
          ]}
        />
        <Tooltip
          labelFormatter={(label) =>
            moment(parseInt(label)).format("YYYY-MM-DD")
          }
        />
        <Legend />
      </LineChart>
    </Flex>
  );
};

export default PpsGraph;
